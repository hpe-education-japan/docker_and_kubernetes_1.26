## Lab5 Docker Network

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
- Private Networkを作成してコンテナで使用する
- 外部からコンテナへ接続する


  
<br>
<br>

## Task1<br>Private Networkの管理  

**1) brctlツールをインストールしておきます**
```
$ sudo apt-get install -y bridge-utils
[sudo] password for student:

Reading package lists... Done
Building dependency tree
Reading state information... Done
Suggested packages:
...

$ brctl show

bridge name     bridge id               STP enabled     interfaces
docker0         8000.024214ab45c7       no
```

**2) 簡単なコンテナを作成してネットワーク設定を確認します。確認後exitします。**
```
$ docker run -ti --rm busybox

/ # ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
4: eth0@if5: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

/ # cat /etc/hosts

127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.2      3637f3ad6e37

/ # cat /etc/resolv.conf

# This file is managed by man:systemd-resolved(8). Do not edit.
#
# This is a dynamic resolv.conf file for connecting local clients directly to
# all known uplink DNS servers. This file lists all configured search domains.
#
# Third party programs must not access this file directly, but only through the
# symlink at /etc/resolv.conf. To manage man:resolv.conf(5) in a different way,
# replace this symlink by a static file or a different symlink.
#
# See man:systemd-resolved.service(8) for details about the supported modes of
# operation for /etc/resolv.conf.

nameserver 10.0.0.2
search ap-northeast-1.compute.internal

/ # exit
```

**3) Private Networkを作成します。作成後確認してみましょう。**
```
$ docker network create net1

b39fd8507161ff38395887071cc46eca5279452d9c541f467a779415f317574e

$ docker network ls

NETWORK ID     NAME      DRIVER    SCOPE
abd547cc9eab   bridge    bridge    local
ffb26222c6fc   host      host      local
b39fd8507161   net1      bridge    local
be35631da096   none      null      local
```

**4) ipコマンドやbrctlでも確認します**
```
$ ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9001 qdisc fq_codel state UP group default qlen 1000
    link/ether 06:df:dd:cc:c0:2b brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.20/24 brd 10.0.0.255 scope global dynamic eth0
       valid_lft 3235sec preferred_lft 3235sec
    inet6 fe80::4df:ddff:fecc:c02b/64 scope link
       valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    link/ether 02:42:14:ab:45:c7 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:14ff:feab:45c7/64 scope link
       valid_lft forever preferred_lft forever
6: br-b39fd8507161: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    link/ether 02:42:ff:3b:93:c9 brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.1/16 brd 172.18.255.255 scope global br-b39fd8507161
       valid_lft forever preferred_lft forever

$ brctl show

bridge name             bridge id               STP enabled     interfaces
br-b39fd8507161         8000.0242ff3b93c9       no
docker0                 8000.024214ab45c7       no              vethd2f5b82
```

**5) ホスト名:host1, コンテナ名:host1 でコンテナを作成します。ネットワークを確認します。**
```
$ docker run --rm -ti -h host1 --name host1 busybox

/ # ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
9: eth0@if10: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

**6) Ctrl+P、Ctrl+Qでコンテナからデタッチします。その後Private Networkをコンテナに接続します。**
```
$ docker network connect net1 host1
```

**7) コンテナにアタッチし直して、ネットワークを確認してみます**
```
$ docker ps

CONTAINER ID   IMAGE     COMMAND   CREATED         STATUS            PORTS     NAMES
b2e5998f237d   busybox   "sh"      3 minutes ago   Up 3 minutes                host1

$ docker attach b2e5998f237d

/ # ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
9: eth0@if10: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
11: eth1@if12: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:12:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.2/16 brd 172.18.255.255 scope global eth1
       valid_lft forever preferred_lft forever
```

**8) Ctrl+P、Ctrl+Qでコンテナからデタッチします。その後host2という名前でコンテナを新しく作成します。作成時にnet1を接続します。**
```
$ docker run --rm -ti -h host2 --name host2 --net=net1 busybox
```

**9) host2のネットワークを確認します。違いは何でしょうか。**
```
/ # ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
13: eth0@if14: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:12:00:03 brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.3/16 brd 172.18.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

**10) hostsも確認しましょう**
```
/ # cat /etc/hosts

127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.18.0.3      host2

```

**11) host1にpingしてみます。自分で作成したPrivate Networkではホスト名でpingが可能です。**
```
/ # ping -c 5 host1

PING host1 (172.18.0.2): 56 data bytes
64 bytes from 172.18.0.2: seq=0 ttl=64 time=0.078 ms
64 bytes from 172.18.0.2: seq=1 ttl=64 time=0.086 ms
64 bytes from 172.18.0.2: seq=2 ttl=64 time=0.086 ms
64 bytes from 172.18.0.2: seq=3 ttl=64 time=0.092 ms
64 bytes from 172.18.0.2: seq=4 ttl=64 time=0.089 ms

--- host1 ping statistics ---
5 packets transmitted, 5 packets received, 0% packet loss
round-trip min/avg/max = 0.078/0.086/0.092 ms
```

**12) exitしてコンテナを削除します**
```
/ # exit

$ docker rm -f $(docker ps -q)
```

**13) Private Networkを削除します**
```
$ docker network rm net1

net1
```

<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab5/Lab5-2.md)

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)

