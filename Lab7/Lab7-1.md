## Lab7 PodとLabel 

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
•	簡単なPodを作成する
•	Labelを設定する

  
<br>
<br>

## Task1<br>Podの作成とLabel設定  

**1) nginxを動かすPodマニフェストを作成します**
```
master:~$ vi pod.yaml
```
[pod.yaml](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab7/labfiles/pod.yaml)

**2) Podを作成します**
```
master:~$ kubectl apply -f pod.yaml

pod/nginx created
```

**3) Podのステータスを確認します**
```
master:~$ kubectl get pods

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          11s
```

**4) Podの動いているノードやIPアドレスを表示してみます**
```
master:~$ kubectl get pods -o wide

NAME    READY   STATUS    RESTARTS   AGE   IP                NODE           MOMINATED  NODE   READINESS GATES
nginx   1/1     Running   0          11m   192.168.215.141   set99-worker   <none>            <none>
```

**5) Podの詳細情報を確認します**
```
master:~$ kubectl describe pod nginx

Name:         nginx
Namespace:    default
Priority:     0
Node:         set99-worker/10.0.0.71
Start Time:   Tue, 09 Nov 2021 16:17:37 +0900
Labels:       <none>
Annotations:  cni.projectcalico.org/containerID: d7ee80ea60c3e365e58f6664712882991d955606949b5bdf9bd2b6407105870b
              cni.projectcalico.org/podIP: 192.168.215.141/32
              cni.projectcalico.org/podIPs: 192.168.215.141/32
Status:       Running
IP:           192.168.215.141
IPs:
  IP:  192.168.215.141
Containers:
  nginx:
    Container ID:   docker://b328a7c14549574861590d06bb6312ba099a6aee6ac2edb296bb817b632b8291
    Image:          nginx:1.14.2
    Image ID:       docker-pullable://nginx@sha256:f7988fb6c02e0ce69257d9bd9cf37ae20a60f1df7563c3a2a6abe24160306b8d
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
...
```

**6) PodのIPアドレスにアクセスしてnginxの動作を確認します**
```
master:~$ curl http://192.168.215.141:80

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>

<h1>Welcome to nginx!</h1>
```

**7) Podのログを表示します**
```
master:~$ kubectl logs nginx

192.168.108.0 - - [09/Nov/2021:07:18:34 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.68.0" "-"
```

**8) 同じnginx PodをCLIで作成してみます**
```
master:~$ kubectl run nginx-cli --image=nginx

pod/nginx-cli created
```

**9) Podのステータスを確認します**
```
master:~$ kubectl get pods

NAME        READY   STATUS    RESTARTS   AGE
nginx       1/1     Running   0          16m
nginx-cli   1/1     Running   0          13s
```

**10) Podのアドレスを確認します**
```
master:~$ kubectl get pods -o wide

NAME        READY   STATUS    RESTARTS   AGE   IP                NODE           NOMINATED NODE   READINESS GATES
nginx       1/1     Running   0          16m   192.168.215.141   set99-worker   <none>           <none>
nginx-cli   1/1     Running   0          22s   192.168.215.142   set99-worker   <none>           <none>
```

**11) CLIで作成したPodのnginxに接続してみます**
```
master:~$ curl http://192.168.215.142:80

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>

<h1>Welcome to nginx!</h1>
```

**12) CLIで作成したPodのログを表示します**
```
master:~$ kubectl logs nginx-cli

/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
...
```

**13) nginx Podを削除します**
```
master:~$ kubectl delete pod nginx

pod "nginx" deleted
```

**14) Labelを追加してみます。podのマニフェストを更新します。**
```
master:~$ vi pod.yaml
```
[pod.yaml](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab7/labfiles/pod-2.yaml)

**15) Podを作成します**
```
master:~$ kubectl apply -f pod.yaml

pod/nginx created
```
**16) Podを確認します**
```
master:~$ kubectl get pods -o wide

NAME        READY   STATUS    RESTARTS   AGE     IP                NODE           NOMINATED NODE   READINESS GATES
nginx       1/1     Running   0          8s      192.168.215.145   set99-worker   <none>           <none>
nginx-cli   1/1     Running   0          2m27s   192.168.215.144   set99-worker   <none>           <none>
```

**17) Labelを確認します。設定していないnginx-cliにもデフォルトのLabelが設定されています。**
```
master:~$ kubectl get pods --show-labels

NAME        READY   STATUS    RESTARTS   AGE      LABELS
nginx       1/1     Running   0          2m33s    app=web,env=prod
nginx-cli   1/1     Running   0          4m52s    run=nginx-cli
```

**18) Labelを使ってPodを表示してみます**
```
master:~$ kubectl get pods -l app=web

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          4m8s

```
**19) nginx-cli Podに手動でLabelを設定します**
```
master:~$ kubectl label pods nginx-cli app=web

pod/nginx-cli labeled
```

**20) Labelを確認します**
```
master:~$ kubectl get pods -l app=web

NAME        READY   STATUS    RESTARTS   AGE
nginx       1/1     Running   0          6m26s
nginx-cli   1/1     Running   0          8m45s
```

**21) Podを削除します**
```
master:~$ kubectl delete pod nginx nginx-cli

pod "nginx" deleted
pod "nginx-cli" deleted
```

<br>
<br>
以上でこのラボは終了です。
<br>
<br>


[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)
