# Lab1<br>Docker Install

**時間:20分**

**目的:**

このラボを完了すると、次のことができるようになります。  

- Docker Engineのインストール
- Containerd コンテナランタイムのインストール


**Notices:**


- 使用するUbuntuサーバーのログイン方法はインストラクターから提供します。

- studentユーザーでdockerコマンドが使用できるようにGroupに追加します。  

- Installの詳細は以下を参照して下さい。  
https://docs.docker.com/engine/install/  
- この演習では「setX-master」ノードだけインストールしてください。    


<br>

**1) 各自アサインされたユーザーでUbuntuサーバーにログインします**

**2) rootユーザーに移行します**
```
$ sudo -i

[sudo] password for student: password
#  
```

**3) Ubuntu OSをアップデートします**

```
# apt-get update  

```
**4) Docker GPG Keyを追加します**
```
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg |sudo gpg --dearmor \
-o /usr/share/keyrings/docker-archive-keyring.gpg

```

**5) Repositoryを追加します**
```
# echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee \
/etc/apt/sources.list.d/docker.list > /dev/null
```

**6) aptをアップデートします**
```
# apt-get update
```

**7) Docker EngineとContainerdをInstallします**

```
# apt-get install -y docker-ce docker-ce-cli containerd.io
```

**8) Installの確認をします**

```
# docker version

Client: Docker Engine - Community
 Version:           24.0.2
 API version:       1.43
 Go version:        go1.20.4
 Git commit:        cb74dfc
 Built:             Thu May 25 21:52:22 2023
 OS/Arch:           linux/amd64
 Context:           default
....


# docker run hello-world

latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:2498fce14358aa50ead0cc6c19990fc6ff866ce72aeb5546e1d59caac3d0d60f
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
```

**9) Dockerサービスを確認してみましょう**
```
# systemctl status docker.service
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2023-06-06 09:30:00 JST; 5h 4min ago
....
```

**10) rootユーザーからexitします**
```
# exit
```

**11) studentユーザーをdockerグループへ追加します**

```
$ sudo usermod -aG docker $USER
```

**12) ログアウト後ログインし直します**

```
$ exit
```

**13) studentユーザーでdockerコマンドを確認します。出力が変った事を確認しましょう。**

```
$ docker run --rm hello-world

....
Hello from Docker!
....
```

**8) 更にdockerコマンドを確認してみましょう**

```
$ docker run --rm docker/whalesay cowsay boo
$ docker run --rm chuanwen/cowsay
```


以上でこのタスクは終了です。

<br>
[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)



