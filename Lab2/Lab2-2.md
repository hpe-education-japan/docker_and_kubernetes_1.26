## Task2<br>既存のコンテナに接続する  

**1) Nginxコンテナをデプロイしておきます。**
```
$ docker run -d --name web1 nginx 
f4179cf64f944bdc98db42e3447f96b12e3c4f5a1aca042a49a8332942da7d80

$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS     NAMES
f4179cf64f94   nginx     "/docker-entrypoint.…"   2 seconds ago   Up 2 seconds   80/tcp    web1

```

**2) Nginxコンテナでコマンドを実行します**
```
$ docker exec web1 env 

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=f4179cf64f94
NGINX_VERSION=1.21.3
NJS_VERSION=0.6.2
PKG_RELEASE=1~buster
HOME=/root 

$ docker exec web1 cat /etc/hosts

127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.2      f4179cf64f94

```

**3) 対話型シェルセッションを起動します。いくつかコマンドを確認しましょう。その後exitします。**
```
$ docker exec -ti web1 /bin/bash

root@f4179cf64f94:/# ls
bin  boot  dev  docker-entrypoint.d  docker-entrypoint.sh  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@f4179cf64f94:/# exit
```

**4) コンテナからファイルをコピーしてみます。Web1からコピーしてみましょう。**
```
$ docker cp web1:/etc/nginx/nginx.conf .

$ ls
centos_7.tgz  nginx.conf  test

```

**5) Testfileを作成してweb1へファイルをコピーします**
```
$ touch testfile
$ docker cp testfile web1:/
$ docker exec web1 ls /
bin
boot
...
testfile
...

```

**6) コンテナを検査します**
```
$ docker inspect web1

[
    {
        "Id": "86cb8218d8d956f794ab8332545e52b222978522f3d8ced37ca86afec9620753",
        "Created": "2021-10-21T07:00:29.325711883Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
            ...

```
何が表示されているか確認してみましょう。  
コンテナのIPアドレスを見つけましょう。  

**7) コンテナのIPアドレスを使ってNginxにアクセスしてみます**
```
$ curl 172.17.0.2

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>  
...

```

<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab2/Lab2-3.md)

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)
