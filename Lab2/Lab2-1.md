## Lab2

**時間:60分**

**目的:**

このラボを完了すると、次のことができるようになります。  
•	起動停止などのコンテナ操作  
•	イメージの表示などの操作やイメージのコミット、バックアップ  
<br>
<br>

## Task1<br>コンテナの起動・停止・削除  

**1) Nginxイメージを使いバックグラウンドでコンテナを実行します。名前を付けます。**
```
$ docker run -d --name web1 nginx
7287427b4bd8494181e4efa2763e000c9cfcd30d3de26e05141d3719216333dc

```


**2) docker psで確認します**
```
$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS     NAMES
f4179cf64f94   nginx     "/docker-entrypoint.…"   2 seconds ago   Up 2 seconds   80/tcp    web1 

```

**3) Ubuntuイメージでコンテナを作成して対話型シェルを開きます。コンテナ名は指定しません。**

```
$ docker run -ti ubuntu

Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
7b1a6ab2e44d: Pull complete
Digest: sha256:626ffe58f6e7566e00254b638eb7e0f3b11d4da9675088f4781a50ae288f3322
Status: Downloaded newer image for ubuntu:latest

```

**4) コンテナのシェルでコマンドを実行します**

```
root@b415ed7554aa:/# cat /etc/os-release

NAME="Ubuntu"
VERSION="20.04.3 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04.3 LTS"
VERSION_ID="20.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=focal
UBUNTU_CODENAME=focal

```
**5) プロセスを確認します。PID=1がbashだと確認できます。**

```
root@b415ed7554aa:/# ps -ef

UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 01:32 pts/0    00:00:00 bash
root          10       1  0 01:33 pts/0    00:00:00 ps -ef
```

**6) Ctrl/P, Ctrl/Qでデタッチします。その後コンテナを確認します。ubuntuコンテナの名前を確認します。**

```
$ docker ps

CONTAINER ID    IMAGE     COMMAND                      CREATED         STATUS          PORTS     NAMES
b415ed7554aa    ubuntu    "bash"                       2 minutes ago   Up 2 minutes              silly_goldwasser
2a970ae98f5b    nginx     "/docker-entrypoint.…"       3 minutes ago   Up 3 minutes    80/tcp    web1

```

**7) CONTAINER IDでプロセスをgrepします**

```
$ ps -ef | grep b415ed7554aa

root        3028       1  0 06:34 ?        00:00:00 /usr/bin/containerd-shim-runc-v2 -namespace moby -id b415ed7554aaaaca36f84dbe7f5ae69c7474feab817b6a82507967236c4214ed -address /run/containerd/containerd.sock
student     3095    1323  0 06:37 pts/0    00:00:00 grep --color=auto b415ed7554aa

```
containerd-shimが確認できます。更にshimのPIDで検索します。bashが確認できます。  
```
$ ps -ef | grep 302

root        3028       1  0 06:34 ?        00:00:00 /usr/bin/containerd-shim-runc-v2 -namespace moby -id b415ed7554aaaaca36f84dbe7f5ae69c7474feab817b6a82507967236c4214ed -address /run/containerd/containerd.sock
root        3049    3028  0 06:34 pts/0    00:00:00 bash
student     3097    1323  0 06:38 pts/0    00:00:00 grep --color=auto 3028

```

**8) web1をstop→startしてみます**

```
$ docker ps

CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS         PORTS     NAMES
b415ed7554aa   ubuntu    "bash"                   11 minutes ago   Up 11 minutes            silly_goldwasser
2a970ae98f5b    nginx    "/docker-entrypoint.…"   11 minutes ago   Up 4 seconds    80/tcp   web1

$ docker stop web1

web1

$ docker ps

CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS              PORTS     NAMES
b415ed7554aa   ubuntu    "bash"    11 minutes ago   Up 11 minutes                 silly_goldwasser

$ docker start web1

web1

$ docker ps

CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS     NAMES
b415ed7554aa   ubuntu    "bash"                   11 minutes ago   Up 11 minutes             silly_goldwasser
2a970ae98f5b   nginx     "/docker-entrypoint.…"   12 minutes ago   Up 9 seconds    80/tcp     web1

```

**9) web1を削除します。エラーになるのでstop後削除します。**
```
$ docker rm web1

Error response from daemon: You cannot remove a running container 2a970ae98f5bd8bcb538b91196f8488fde0ae91bfdce794c6245a610b5ab1452. Stop the container before attempting removal or force remove

$ docker stop web1

web1

$ docker ps

CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS               PORTS     NAMES
b415ed7554aa   ubuntu    "bash"    12 minutes ago   Up 12 minutes                  silly_goldwasser

$ docker rm web1

web1
```

**10) 10.	ubuntuコンテナを削除します。-f オプションで実行中でも削除できます。**
```
$ docker rm -f silly_goldwasser

silly_goldwasser

```
<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab2/Lab2-2.md)

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)  

