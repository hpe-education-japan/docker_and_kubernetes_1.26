## Lab9 Volume 

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
•	Volumeを理解する

  
<br>
<br>

## Task1<br>emptyDirを使ったPod内コンテナのファイル共有  

**1) emptyDirを共有するPodのマニフェストを作成します。redisコンテナでは/data/redis, nginxコンテナでは/usr/share/nginx/htmlにマウントします。**
```
master:~$ vi storage.yaml
```
[storage.yaml](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab9/labfiles/storage.yaml)

**2) Podを起動します**
```
master:~$ kubectl apply -f storage.yaml

pod/two-containers created
```

**3) Podを確認します**
```
master:~$ kubectl get pods

NAME                  READY   STATUS    RESTARTS   AGE
two-containers        2/2     Running   0          4m29s

master:~$ kubectl describe pod two-containers

Name:         two-containers
Namespace:    default
Priority:     0
...
```

**4) redisコンテナでシェルを実行します**
```
master:~$ kubectl exec -it two-containers -c redis -- /bin/bash

root@two-containers:/data#
```

**5) コンテナでファイルを作成します**
```
root@two-containers:/data# cd /data/redis

root@two-containers:/data/redis# echo "from redis" > from-redis

root@two-containers:/data/redis# cat from-redis

from redis
```

**6) コンテナからexitします**
```
root@two-containers:/data/redis# exit

exit
```

**7) nginxコンテナでシェルを実行します**
```
master:~$ kubectl exec -it two-containers -c nginx -- /bin/bash

root@two-containers:/#
```

**8) ファイルを確認します**
```
root@two-containers:/# cd /usr/share/nginx/html

root@two-containers:/usr/share/nginx/html# ls

from-redis

root@two-containers:/usr/share/nginx/html# cat from-redis

from redis
```

**9) コンテナからexitします**
```
root@two-containers:/usr/share/nginx/html# exit
```

<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab9/Lab9-2.md)  
  
[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)  

