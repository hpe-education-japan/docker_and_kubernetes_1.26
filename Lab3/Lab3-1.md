## Lab3 

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
•	Dockerfileを使ったコンテナイメージのビルド  
•	イメージのレイヤーとキャッシュを使う  
<br>
<br>

## Task1<br>Dockerfileを使ったイメージビルドとレイヤー、キャッシュの確認  

**1) ここまでに作成したコンテナとイメージを削除します**
```
$ docker container stop $(docker ps -aq) 
$ docker system prune -af
```

**2) イメージビルド用のディレクトリを作成します**
```
$ mkdir build
$ cd build
```

**3) コピー時に参照するファイルを作成します**
```
$ echo data1 > file1
$ echo data2 > file2
```

**4) Dockerfileを作成します**
```
$ vi Dockerfile
```
[Dockerfile](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/labfiles/Dockerfile_1.yaml)

**5) イメージをビルドします**
```
$ docker image build .


[+] Building 0.2s (5/5) FINISHED
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 87B
 => [internal] load build context
 => => transferring context: 38B
 => [1/1] COPY file1 /
 => exporting to image
 => => exporting layers
 => => writing image sha256:356f8e62234395f58fe4add028e4dcdab8eaae6fe768b21e2f122e00
```

**6) 参考：進行状況のステップを表示するには　--progress=plain　オプションを使います**
```
$ docker image build --progress=plain .


#1 [internal] load .dockerignore
#1 transferring context: 2B done
#1 DONE 0.0s

#2 [internal] load build definition from Dockerfile
#2 transferring dockerfile: 86B done
#2 DONE 0.0s

#3 [internal] load build context
#3 transferring context: 26B done
#3 DONE 0.0s

#4 [1/1] COPY file1 /
#4 CACHED

#5 exporting to image
#5 exporting layers done
#5 writing image sha256:356f8e62234395f58fe4add028e4dcdab8eaae6fe768b21e2f122e00b5e713cb done
#5 DONE 0.0s
```

**7) 作成されたイメージを確認します。名前とTAGが\<none\>になっています。これがダングリングイメージです。**
```
$ docker image ls

REPOSITORY   TAG       IMAGE ID       CREATED          SIZE
<none>       <none>    44a30131cf90   29 seconds ago   6B
```

**8) 再度イメージをビルドします。今回はTAGを指定します。各Stepでキャッシュが使われたか確認してください。処理時間が早くなった事も確認できます。出力確認のため　--progressオプションを使いましょう。**
```
$ docker image build --progress=plain -t image:v1 .


#1 [internal] load .dockerignore
#1 transferring context: 2B done
#1 DONE 0.0s

#2 [internal] load build definition from Dockerfile
#2 transferring dockerfile: 86B done
#2 DONE 0.0s

#3 [internal] load build context
#3 transferring context: 26B done
#3 DONE 0.0s

#4 [1/1] COPY file1 /
#4 CACHED

#5 exporting to image
#5 exporting layers done
#5 writing image sha256:356f8e62234395f58fe4add028e4dcdab8eaae6fe768b21e2f122e00b5e713cb done
#5 naming to docker.io/library/image:v1 done
#5 DONE 0.0s
```

**9) イメージを確認します。キャッシュされたレイヤーを使用してTAGが設定されました。**
```
$ docker image ls

REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
image        v1        44a30131cf90   2 minutes ago   6B
```

**10) Dockerfileを変更します。COPY file2 /　を追加します。**

[Dockerfile](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/labfiles/Dockerfile_2.yaml)
<br>

**11) イメージをビルドします。別のTAG名を指定します。キャッシュが使われているか確認します。出力確認のため　--progressオプションを使いましょう。**
```
$ docker image build --progress=plain -t image:v2 .


#1 [internal] load .dockerignore
#1 transferring context: 2B done
#1 DONE 0.0s

#2 [internal] load build definition from Dockerfile
#2 transferring dockerfile: 99B done
#2 DONE 0.0s

#3 [internal] load build context
#3 transferring context: 66B done
#3 DONE 0.0s

#4 [1/2] COPY file1 /
#4 CACHED

#5 [2/2] COPY file2 /
#5 DONE 0.0s

#6 exporting to image
#6 exporting layers done
#6 writing image sha256:ef391396c106bbf6659a7a493f121c64c105bbf427ee6cd16e3777b754d08b1f done
#6 naming to docker.io/library/image:v2 done
#6 DONE 0.0s
```

**12) イメージのレイヤーを確認します。COPY命令ごとに2つのレイヤーが作成されています。**
```
$ docker history image:v2

IMAGE          CREATED       CREATED BY                SIZE      COMMENT
ef391396c106   5 hours ago   COPY file2 / # buildkit   6B        buildkit.dockerfile.v0
<missing>      6 hours ago   COPY file1 / # buildkit   6B        buildkit.dockerfile.v0
<missing>      6 hours ago   MAINTAINER "your_name"    0B        buildkit.dockerfile.v0
```

**13) COPY命令を1つにするようにDockerfileを変更します**

[Dockerfile](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/labfiles/Dockerfile_3.yaml)
<br>

**14) イメージをビルドします。ステップが少なくなっている事を確認します。キャッシュについても確認します。出力確認のため　--progressオプションを使いましょう。**
```
$ docker image build --progress=plain -t image:v3 .


#1 [internal] load .dockerignore
#1 transferring context: 2B done
#1 DONE 0.0s

#2 [internal] load build definition from Dockerfile
#2 transferring dockerfile: 86B done
#2 DONE 0.0s

#3 [internal] load build context
#3 transferring context: 50B done
#3 DONE 0.0s

#4 [1/1] COPY file* /
#4 DONE 0.0s

#5 exporting to image
#5 exporting layers done
#5 writing image sha256:b6f12af4c7b2e5e1bd7b2365c6083bc6eaa911dcd520cb62e417a6c2e47054a1 done
#5 naming to docker.io/library/image:v3 done
#5 DONE 0.0s
```

**15) 作成されたイメージのレイヤーを確認します**
```
$ docker image history image:v3

IMAGE          CREATED       CREATED BY                SIZE      COMMENT
b6f12af4c7b2   5 hours ago   COPY file* / # buildkit   12B       buildkit.dockerfile.v0
<missing>      5 hours ago   MAINTAINER "your_name"    0B        buildkit.dockerfile.v0
```
<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/Lab3-2.md)

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)
