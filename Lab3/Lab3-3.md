## Task3<br>Apacheを起動する  

Base ImageにUbuntuを使い、Build時にApacheをインストールしてコンテナ実行時にhttpdを起動するコンテナイメージを作成します。  

**1) 新しくビルド用ディレクトリを作成します**
```
$ cd ~
$ mkdir httpd
$ cd httpd
```

**2) Dockerfileを作成します**
```
$ vi Dockerfile
```
[Dockerfile](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/labfiles/Dockerfile_5.yaml)
<br>

**3) イメージをビルドします**
```
$ docker image build -t my_httpd .

[+] Building 37.4s (7/7) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 170B
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/ubuntu:latest
 => [auth] library/ubuntu:pull token for registry-1.docker.io
....
```

**4) コンテナを起動します**
```
$ docker run -d --rm -p 80:80 my_httpd

e31390bc16e936d114661296b36bbb7211a91c55e150104dd1003640110eb44f
```

**5) ブラウザから演習用VMの外部IPアドレスにアクセスします**
```
http://< VMの外部IPアドレス>
```
![Lab3-1-2](/uploads/d1b1ea738f5942643cb4e8b95be4f2bc/Lab3-1-2.png)

**6) ホームディレクトリに戻っておきましょう。また、作成したコンテナを削除しておきましょう。**
```
$ cd ~
$ docker container rm -f $(docker ps -aq)
```

<br>
<br>
以上でこのラボは終了です。
<br>
<br>
[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)

