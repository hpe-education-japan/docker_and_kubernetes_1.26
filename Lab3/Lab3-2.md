## Task2<br>Dockerfile命令を使う  

Base Imageにalpineを使い、Build時に実行イメージhelloを作成して、コンテナ実行時にイメージを起動するコンテナイメージを作成します。  

**1) 新しくビルド用ディレクトリを作成します**
```
$ cd ~
$ mkdir hello
$ cd hello
```

**2) hello.cを作成します**
```
$ vi hello.c
```

[hello.c](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/labfiles/hello.c)
<br>

**3) Dockerfileを作成します**
```
$ vi Dockerfile
```
[Dockerfile](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/labfiles/Dockerfile_4.yaml)
<br>

**4) イメージをビルドします**
```
$ docker image build -t hello:v1 .

[+] Building 9.0s (10/10) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 161B 
 => [internal] load .dockerignore
 => => transferring context: 2B      
 => [internal] load metadata for docker.io/library/alpine:3.9
 => [auth] library/alpine:pull token for registry-1.docker.io 0.0s
 => [1/4] FROM
....
```

**5) イメージを確認します**
```
$ docker image ls hello:v1

REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
hello        v1        1708bb6e541a   2 minutes ago   187MB
```

**6) コンテナの動作を確認します**
```
$ docker container run --rm hello:v1

Hello, world!
```
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab3/Lab3-3.md)

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)
