## Lab4 Docker Volumeの操作

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
- Docker Volumeを作成してコンテナで使用する
- 名前付きボリュームを使用する
- 匿名ボリュームを使用する
- 外部ボリュームを使用する

  
<br>
<br>

## Task1<br>名前付きボリュームの作成  

**1) 名前付きボリュームを作成します**
```
$ docker volume create datavol1

datavol1
```

**2) 作成したボリュームの詳細を確認します**
```
$ docker volume inspect datavol1

[
    {
        "CreatedAt": "2021-10-14T07:03:07Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/datavol1/_data",
        "Name": "datavol1",
        "Options": {},
        "Scope": "local"
    }
]
```

**3) busyboxイメージでコンテナを作成して、それにボリュームをアタッチします**
```
$ docker container run -v datavol1:/data -ti --rm busybox

Unable to find image 'busybox:latest' locally
latest: Pulling from library/busybox
24fb2886d6f6: Pull complete
Digest: sha256:f7ca5a32c10d51aeda3b4d01c61c6061f497893d7f6628b92f822f7117182a57
Status: Downloaded newer image for busybox:latest
/ #
```

**4) コンテナの /data ディレクトリにボリュームがアタッチされました。コンテナでファイルを作成してみます。**
```
/ # echo "example data" > /data/file
/ # cat /data/file

example data

/ # exit
```

**5) Docker Hostから作成したファイルを確認します**
```
$ sudo cat /var/lib/docker/volumes/datavol1/_data/file
[sudo] password for student:

example data
```

**6) 作成されているボリュームをリスト表示します**
```
$ docker volume ls

DRIVER    VOLUME NAME
local     datavol1
```

**7) 匿名ボリュームを作成します。作成後確認してみましょう。**
```
$ docker volume create

8eff73a9a488e595234a50c6a5dd21950b29bee2f46496dbc0a4ce04da250124

$ docker volume ls

DRIVER        VOLUME NAME
local         8eff73a9a488e595234a50c6a5dd21950b29bee2f46496dbc0a4ce04da250124
local         datavol1

$ docker volume inspect 8eff73a9a488e595234a50c6a5dd21950b29bee2f46496dbc0a4ce04da250124

[
    {
        "CreatedAt": "2021-10-22T07:17:55Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/8eff73a9a488e595234a50c6a5dd21950b29bee2f46496dbc0a4ce04da250124/_data",
        "Name": "8eff73a9a488e595234a50c6a5dd21950b29bee2f46496dbc0a4ce04da250124",
        "Options": {},
        "Scope": "local"
    }
]
```

<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab4/Lab4-2.md)

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)
