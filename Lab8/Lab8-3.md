## Task3<br>DaemonSet  

**1) DaemonSetのマニフェストを作成します。kuardイメージを使います。**
```
master:~$ vi ds.yaml
```
[ds.yaml](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab8/labfiles/ds.yaml)

**2) DaemonSetを作成します**
```
master:~$ kubectl apply -f ds.yaml

daemonset.apps/kuard created
```

**3) DaemonSetの状態を確認します**
```
master:~$ kubectl get ds

NAME    DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
kuard   2         2         2       2            2           <none>          7s

master:~$ kubectl get ds -o wide

NAME   DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE   CONTAINERS IMAGES                           SELECTOR
kuard  2         2         2       2            2           <none>          11s   kuard      gcr.io/kuar-demo/kuard-amd64:2   app=kuard

```

**4) PodがどのNodeで動いているかを見てみましょう**
```
master:~$ kubectl get pods -o wide

NAME           READY   STATUS    RESTARTS   AGE   IP                NODE           NOMINATED  NODE  READINESS GATES
kuard-dw249    1/1     Running   0          22s   192.168.215.165   set99-worker   <none>           <none>
kuard-vpwk2    1/1     Running   0          22s   192.168.108.7     set99-master   <none>           <none>
```

<br>
<br>
以上でこのラボは終了です。
<br>
<br>
[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)
