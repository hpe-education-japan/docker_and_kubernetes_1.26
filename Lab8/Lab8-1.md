## Lab8 ReplicaSet、Deployment、DaemonSet 

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
•	ReplicaSetとPodの関係を理解する
•	DeploymentとReplicaSetの関係を理解する
•	DaemonSetの動作を理解する


  
<br>
<br>

## Task1<br>ReplicaSet  


**1) ReplicaSetのマニフェストを作成します。kuardイメージを使ってレプリカ数を3にします。**
```
master:~$ vi rs.yaml
```
[rs.yaml](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab8/labfiles/rs.yaml)

**2) ReplicaSetを作成します**
```
master:~$ kubectl apply -f rs.yaml

replicaset.apps/kuard created
```

**3) 作成されたReplicaSetとPodを確認します。Pod名を確認しましょう。**
```
$ kubectl get rs

NAME    DESIRED   CURRENT   READY   AGE
kuard   3         3         3       10s

master:~$ kubectl get pods

NAME          READY   STATUS    RESTARTS   AGE
kuard-msvmv   1/1     Running   0          5s
kuard-ssn2d   1/1     Running   0          5s
kuard-wwgsp   1/1     Running   0          6s
```

**4) ReplicaSetの詳細を確認します**
```
master:~$ kubectl describe rs

Name:         kuard
Namespace:    default
Selector:     app=kuard
Labels:       <none>
Annotations:  <none>
Replicas:     3 current / 3 desired
Pods Status:  3 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=kuard
           version=2
  Containers:
   kuard:
    Image:        gcr.io/kuar-demo/kuard-amd64:2
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  17s   replicaset-controller  Created pod: kuard-wwgsp
  Normal  SuccessfulCreate  16s   replicaset-controller  Created pod: kuard-ssn2d
  Normal  SuccessfulCreate  16s   replicaset-controller  Created pod: kuard-msvmv
```

**5) replicas=4に設定して手動でスケールアップしてみます**
```
master:~$ kubectl scale rs kuard --replicas=4

replicaset.apps/kuard scaled

master:~$ kubectl get pods

NAME          READY   STATUS    RESTARTS   AGE
kuard-b2jk7   1/1     Running   0          10s
kuard-msvmv   1/1     Running   0          4m22s
kuard-ssn2d   1/1     Running   0          4m22s
kuard-wwgsp   1/1     Running   0          4m23s
```

**6) Podがスケールされました。ここで先ほど作成したReplicaSetのマニフェストを適用してみます。**
```
master:~$ kubectl apply -f rs.yaml

replicaset.apps/kuard configured
```

**7) Podの状態を観察してみましょう。マニフェストで設定されたreplicas=3に戻る事がわかります。**
```
master:~$ kubectl get pods

NAME          READY   STATUS    RESTARTS   AGE
kuard-msvmv   1/1     Running   0          6m15s
kuard-ssn2d   1/1     Running   0          6m15s
kuard-wwgsp   1/1     Running   0          6m16s
```

**8) PodをReplicaSetから分離してみます。1つのPodのLabelを変更しましょう。**
```
master:~$ kubectl edit pod kuard-wwgsp

apiVersion: v1
kind: Pod
metadata:
  annotations:
    cni.projectcalico.org/containerID: 02b6f1cb9bb06c8959c61c9ecda59101723b5d9132c3929c43c300eae6bd08dc
    cni.projectcalico.org/podIP: 192.168.215.150/32
    cni.projectcalico.org/podIPs: 192.168.215.150/32
  creationTimestamp: "2021-11-10T05:27:21Z"
  generateName: kuard-
  labels:
    app: kuard　　←kuard-otherに変更
    version: "2"
```

**9) Podの状態を確認します **
```
master:~$ kubectl get pods

NAME          READY   STATUS    RESTARTS   AGE
kuard-msvmv   1/1     Running   0          17m
kuard-n88fj   1/1     Running   0          44s
kuard-ssn2d   1/1     Running   0          17m
kuard-wwgsp   1/1     Running   0          17m
```

**10) PodのLabelを確認します**
```
master:~$ kubectl get pods --show-labels

NAME          READY   STATUS    RESTARTS   AGE   LABELS
kuard-msvmv   1/1     Running   0          17m   app=kuard,version=2
kuard-n88fj   1/1     Running   0          63s   app=kuard,version=2
kuard-ssn2d   1/1     Running   0          17m   app=kuard,version=2
kuard-wwgsp   1/1     Running   0          17m   app=kuard-other,version=2

master:~$ kubectl get pods -l app=kuard

NAME          READY   STATUS    RESTARTS   AGE
kuard-msvmv   1/1     Running   0          18m
kuard-n88fj   1/1     Running   0          119s
kuard-ssn2d   1/1     Running   0          18m
```

**11) ReplicaSetを削除します**
```
master:~$ kubectl delete rs kuard

replicaset.apps "kuard" deleted
```

**12) Podの状態を確認します。ReplicaSetから分離されたPodが残っています。**
```
master:~$ kubectl get pods

NAME          READY   STATUS    RESTARTS   AGE
kuard-wwgsp   1/1     Running   0          19m
```

**13) Podを削除します**
```
master:~$ kubectl delete pod kuard-wwgsp

pod "kuard-wwgsp" deleted
```
<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26/-/blob/master/Lab8/Lab8-2.md)  

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)

