## Task2<br>Kubernetes Cluster Worker Node Setup  

**1) ワーカーノードにログインしてrootにスイッチします**
```
worker:~$ sudo -i
[sudo] password for student:
```

**2) OSをアップデートします**
```
worker:~# apt-get update && apt-get upgrade -y
```

**3) コントロールプレーンノードと同様の手順で必要なソフトウェア及びcontainerdをインストールします。**
```
worker:~# apt install curl apt-transport-https vim git wget gnupg2 \
software-properties-common ca-certificates uidmap -y

worker:~# swapoff -a

worker:~# modprobe overlay

worker:~# modprobe br_netfilter

worker:~# cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF


worker:~# sysctl --system

worker:~# mkdir -p /etc/apt/keyrings

worker:~# curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
| sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

worker:~# echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


worker:~# apt-get update && apt-get install containerd.io -y

worker:~# containerd config default | tee /etc/containerd/config.toml

worker:~# sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml

worker:~# systemctl restart containerd


```

**4) Kubernetes 用に新しいリポジトリを追加します。/etc/apt/sources.list.d/kubernetes.list を作成して1行追加します。**
```
worker:~# vi /etc/apt/sources.list.d/kubernetes.list
```
[kubernetes.list](https://gitlab.com/hpe-education-japan/docker_and_kubernetes/-/blob/master/Lab5/labfiles/kubernetes.list)

**5) パッケージ用に GPG キーを追加します**
```
worker:~# curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

OK
```

**6) 記述したリポジトリを更新します**
```
worker:~# apt-get update

Hit:1 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal InRelease
Hit:2 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-updates InRelease
Hit:3 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-backports InRelease
Hit:5 http://security.ubuntu.com/ubuntu focal-security InRelease
Get:4 https://packages.cloud.google.com/apt kubernetes-xenial InRelease [9383 B]
Get:6 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 Packages [50.9 kB]
Fetched 60.3 kB in 1s (41.7 kB/s)
Reading package lists... Done
```

**7) kubeadm, kubelet, kubectl ソフトウェアをインストールします。バージョン1.26.1を指定します。**
```
worker:~# apt-get install -y kubeadm=1.26.1-00 kubelet=1.26.1-00 kubectl=1.26.1-00

Reading package lists... Done
Building dependency tree
Reading state information... Done
...
```

**8) バージョンを固定します**
```
worker:~#  apt-mark hold kubelet kubeadm kubectl

kubelet set on hold.
kubeadm set on hold.
kubectl set on hold.
```

**9) hostsファイルのsetXX-masterにk8scpエイリアスを追加します**
```
worker:~# vi /etc/hosts

例：  
10.0.0.30       set99-master k8scp  
10.0.0.71       set99-worker  
```

**10) マスターノード設定時に表示されたJoinコマンドを実行します**
```
worker:~# kubeadm join k8scp:6443 --token 46fq07.eaiu2i7dnh8aq4zw \
>         --discovery-token-ca-cert-hash sha256:a396a9d25724a67aae636bc55d83e9e11c337bbc6bce41ac950e6aa6bc0f845a

[preflight] Running pre-flight checks
        [WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
```

**11) rootユーザーから抜けます**
```
worker:~# exit

logout
```

**12) マスターノードに戻りクラスターノードが追加されてReadyになっている事を確認します。数分待つかもしれません。**
```
master:~$ kubectl get nodes

NAME           STATUS   ROLES                  AGE    VERSION
set99-master   Ready    control-plane,master   35m    v1.26.1
set99-worker   Ready    <none>                 75s    v1.26.1
```

**13) この後の演習のためTaintを削除します**
```
master:~$ kubectl describe node | grep -i taint

Taints:             node-role.kubernetes.io/master:NoSchedule
Taints:             <none>

master:~$ kubectl taint nodes --all node-role.kubernetes.io/control-plane:NoSchedule-

node/set99-master untainted
error: taint "node-role.kubernetes.io/control-plane:NoSchedule" not found

master:~$ kubectl describe node | grep -i taint

Taints:             <none>
Taints:             <none>
```

**14) セットアップは完了しました。動作確認しましょう。**
```
master:~$ kubectl run whaleme --image=docker/whalesay --restart=Never --rm -it  -- sh -c "cowsay Hello!"
master:~$ kubectl run whaleme --image=docker/whalesay --restart=Never --rm -it  -- sh -c "cowsay -f elephant Hello!"
master:~$ kubectl run whaleme --image=docker/whalesay --restart=Never --rm -it  -- sh -c "cowsay -f dragon-and-cow Hello!"
```
<br>
<br>
以上でこのラボは終了です。  

<br>

[Top](https://gitlab.com/hpe-education-japan/docker_and_kubernetes_1.26)
